# fastfetch

Like Neofetch, but much faster because written in C

https://github.com/LinusDierheimer/fastfetch

<br>

How to clone this repository:
```
git clone https://gitlab.com/rebornos-team/rebornos-packages/fastfetch.git
```

